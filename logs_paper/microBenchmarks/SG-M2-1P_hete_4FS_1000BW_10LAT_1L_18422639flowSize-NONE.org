#+TITLE: /tmp/logs_microBenchmarks//SG-M2-1P_hete_4FS_1000BW_10LAT_1L_18422639flowSize-NONE.org
#+DATE: Wed Dec 19 10:25:16 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-7.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 35956 172.16.98.7 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 35956 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-4cf07489-e2f8-40df-8ec5-bbfedfa38ad1.xml 4 1 18422639 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link11" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link12" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link13" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>

		<route src="Node0" dst="Node1" symmetrical="NO">
			<link_ctn id="Link11_UP" />
			<link_ctn id="Link12_UP" />
			<link_ctn id="Link13_UP" />
		</route>
		<route src="Node1" dst="Node0" symmetrical="NO">
			<link_ctn id="Link11_DOWN" />
			<link_ctn id="Link12_DOWN" />
			<link_ctn id="Link13_DOWN" />
		</route>
	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 4
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(2) 2.941172] [simulator/INFO] Node0 sent 18422639 bytes to Node1 in 2.941172 seconds from 0.000000 to 2.941172 with flow id 2
[Node0:Node0:(1) 2.941172] [simulator/INFO] Node0 sent 18422639 bytes to Node1 in 2.941172 seconds from 0.000000 to 2.941172 with flow id 1
[Node1:Node1:(4) 2.941172] [simulator/INFO] Node1 sent 18422639 bytes to Node0 in 2.941172 seconds from 0.000000 to 2.941172 with flow id 3
[2.941172] [link_energy/INFO] Total energy over all links: 5.978274
[2.941172] [simulator/INFO] ----- Simulation end -----
[2.941172] [link_energy/INFO] Energy consumption of link 'Link11_DOWN': 1.721933 Joules
[2.941172] [link_energy/INFO] Energy consumption of link 'Link11_UP': 1.786600 Joules
[2.941172] [link_energy/INFO] Energy consumption of link 'Link12_DOWN': 0.000000 Joules
[2.941172] [link_energy/INFO] Energy consumption of link 'Link12_UP': 0.000000 Joules
[2.941172] [link_energy/INFO] Energy consumption of link 'Link13_DOWN': 1.097509 Joules
[2.941172] [link_energy/INFO] Energy consumption of link 'Link13_UP': 1.372231 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.01 wallClock:0.10 peakMemUsage:61896 latency:10 execTime:0.09 simTime:2.941172 totalEnergy:5.978274 totalByteSend:0 flowSize:18422639 flowScenario:4 BW:1000 loop:1 idlecons: byteCons: consType:hete platform:1 simu:SG energyModel:M2 key:NONE


