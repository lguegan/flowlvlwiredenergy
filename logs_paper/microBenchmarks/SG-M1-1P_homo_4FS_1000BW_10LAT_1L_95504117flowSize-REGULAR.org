#+TITLE: /tmp/logs_microBenchmarks//SG-M1-1P_homo_4FS_1000BW_10LAT_1L_95504117flowSize-REGULAR.org
#+DATE: Wed Dec 19 10:13:05 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapluie-38.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 41820 172.16.99.38 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=17
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 41820 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-2291a543-d46d-41f4-a6a9-bd92ba09452c.xml 4 1 95504117 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.002868" />
		</link>

	<route src="Node0" dst="Node1" symmetrical="NO">
		<link_ctn id="Link1_UP" />
	</route>
	<route src="Node1" dst="Node0" symmetrical="NO">
		<link_ctn id="Link1_DOWN" />
	</route>



	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 4
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(2) 14.702872] [simulator/INFO] Node0 sent 95504117 bytes to Node1 in 14.702872 seconds from 0.000000 to 14.702872 with flow id 2
[Node0:Node0:(1) 14.702872] [simulator/INFO] Node0 sent 95504117 bytes to Node1 in 14.702872 seconds from 0.000000 to 14.702872 with flow id 1
[Node1:Node1:(4) 14.702872] [simulator/INFO] Node1 sent 95504117 bytes to Node0 in 14.702872 seconds from 0.000000 to 14.702872 with flow id 3
[14.702872] [link_energy/INFO] Total energy over all links: 35.078204
[14.702872] [simulator/INFO] ----- Simulation end -----
[14.702872] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 17.215835 Joules
[14.702872] [link_energy/INFO] Energy consumption of link 'Link1_UP': 17.862369 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.15 cpuTimeS:0.07 wallClock:1.18 peakMemUsage:61444 latency:10 execTime:0.22 simTime:14.702872 totalEnergy:35.078204 totalByteSend:0 flowSize:95504117 flowScenario:4 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:1 simu:SG energyModel:M1 key:REGULAR


