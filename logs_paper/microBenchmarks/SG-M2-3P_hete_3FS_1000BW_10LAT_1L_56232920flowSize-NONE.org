#+TITLE: /tmp/logs_microBenchmarks//SG-M2-3P_hete_3FS_1000BW_10LAT_1L_56232920flowSize-NONE.org
#+DATE: Wed Dec 19 10:20:02 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-4.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 41178 172.16.98.4 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 41178 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-67ab219d-bcb2-464e-a6bd-cb0f7a29946a.xml 3 1 56232920 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node2" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node3" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node4" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node5" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link11" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link12" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link13" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		
		<link id="Link21" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>
		<link id="Link22" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link23" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		
		<link id="Link31" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link32" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link33" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>
		
		<link id="Link41" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>
		<link id="Link42" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link43" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		
		<link id="Link51" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>
		<link id="Link52" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link53" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.265:2.140337" />
		</link>

		<!-- ---------------- -->
		<route src="Node0" dst="Node4" symmetrical="NO">
			<link_ctn id="Link11_UP" />
			<link_ctn id="Link12_UP" />
			<link_ctn id="Link13_UP" />

			<link_ctn id="Link31_UP" />
			<link_ctn id="Link32_UP" />
			<link_ctn id="Link33_UP" />

			<link_ctn id="Link41_UP" />
			<link_ctn id="Link42_UP" />
			<link_ctn id="Link43_UP" />

		</route>
		<route src="Node4" dst="Node0" symmetrical="NO">
			<link_ctn id="Link11_DOWN" />
			<link_ctn id="Link12_DOWN" />
			<link_ctn id="Link13_DOWN" />

			<link_ctn id="Link31_DOWN" />
			<link_ctn id="Link32_DOWN" />
			<link_ctn id="Link33_DOWN" />

			<link_ctn id="Link41_DOWN" />
			<link_ctn id="Link42_DOWN" />
			<link_ctn id="Link43_DOWN" />

		</route>

		<route src="Node1" dst="Node5" symmetrical="NO">
			<link_ctn id="Link21_UP" />
			<link_ctn id="Link22_UP" />
			<link_ctn id="Link23_UP" />

			<link_ctn id="Link31_UP" />
			<link_ctn id="Link32_UP" />
			<link_ctn id="Link33_UP" />

			<link_ctn id="Link51_UP" />
			<link_ctn id="Link52_UP" />
			<link_ctn id="Link53_UP" />

		</route>
		<route src="Node5" dst="Node1" symmetrical="NO">
			<link_ctn id="Link21_DOWN" />
			<link_ctn id="Link22_DOWN" />
			<link_ctn id="Link23_DOWN" />

			<link_ctn id="Link31_DOWN" />
			<link_ctn id="Link32_DOWN" />
			<link_ctn id="Link33_DOWN" />

			<link_ctn id="Link51_DOWN" />
			<link_ctn id="Link52_DOWN" />
			<link_ctn id="Link53_DOWN" />
		</route>






	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 3
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node4:Node4:(3) 26.131688] [simulator/INFO] Node4 sent 56232920 bytes to Node0 in 26.131688 seconds from 0.000000 to 26.131688 with flow id 2
[Node0:Node0:(1) 26.131688] [simulator/INFO] Node0 sent 56232920 bytes to Node4 in 26.131688 seconds from 0.000000 to 26.131688 with flow id 1
[Node5:Node5:(7) 26.131688] [simulator/INFO] Node5 sent 56232920 bytes to Node1 in 26.131688 seconds from 0.000000 to 26.131688 with flow id 4
[Node1:Node1:(5) 26.131688] [simulator/INFO] Node1 sent 56232920 bytes to Node5 in 26.131688 seconds from 0.000000 to 26.131688 with flow id 3
[26.131688] [link_energy/INFO] Total energy over all links: 228.917642
[26.131688] [simulator/INFO] ----- Simulation end -----
[26.131688] [link_energy/INFO] Energy consumption of link 'Link11_DOWN': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link11_UP': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link12_DOWN': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link12_UP': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link13_DOWN': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link13_UP': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link21_DOWN': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link21_UP': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link22_DOWN': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link22_UP': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link23_DOWN': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link23_UP': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link31_DOWN': 15.057097 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link31_UP': 15.057097 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link32_DOWN': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link32_UP': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link33_DOWN': 8.723415 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link33_UP': 8.723415 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link41_DOWN': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link41_UP': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link42_DOWN': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link42_UP': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link43_DOWN': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link43_UP': 14.845421 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link51_DOWN': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link51_UP': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link52_DOWN': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link52_UP': 0.000000 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link53_DOWN': 7.824156 Joules
[26.131688] [link_energy/INFO] Energy consumption of link 'Link53_UP': 7.824156 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.07 cpuTimeS:0.02 wallClock:0.10 peakMemUsage:60636 latency:10 execTime:0.09 simTime:26.131688 totalEnergy:228.917642 totalByteSend:0 flowSize:56232920 flowScenario:3 BW:1000 loop:1 idlecons: byteCons: consType:hete platform:3 simu:SG energyModel:M2 key:NONE


