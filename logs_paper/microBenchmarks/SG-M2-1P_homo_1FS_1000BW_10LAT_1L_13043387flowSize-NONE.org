#+TITLE: /tmp/logs_microBenchmarks//SG-M2-1P_homo_1FS_1000BW_10LAT_1L_13043387flowSize-NONE.org
#+DATE: Wed Dec 19 10:13:06 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-3.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 45204 172.16.98.3 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 45204 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-704f1bf4-6dea-4c80-ab32-aa68124b74a6.xml 1 1 13043387 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link11" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link12" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link13" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>

		<route src="Node0" dst="Node1" symmetrical="NO">
			<link_ctn id="Link11_UP" />
			<link_ctn id="Link12_UP" />
			<link_ctn id="Link13_UP" />
		</route>
		<route src="Node1" dst="Node0" symmetrical="NO">
			<link_ctn id="Link11_DOWN" />
			<link_ctn id="Link12_DOWN" />
			<link_ctn id="Link13_DOWN" />
		</route>
	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 1
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(1) 2.120363] [simulator/INFO] Node0 sent 13043387 bytes to Node1 in 2.120363 seconds from 0.000000 to 2.120363 with flow id 1
[2.120363] [link_energy/INFO] Total energy over all links: 4.852667
[2.120363] [simulator/INFO] ----- Simulation end -----
[2.120363] [link_energy/INFO] Energy consumption of link 'Link11_DOWN': 1.189857 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link11_UP': 1.236477 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link12_DOWN': 0.000000 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link12_UP': 0.000000 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link13_DOWN': 1.189857 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link13_UP': 1.236477 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.09 cpuTimeS:0.00 wallClock:0.10 peakMemUsage:60532 latency:10 execTime:0.09 simTime:2.120363 totalEnergy:4.852667 totalByteSend:0 flowSize:13043387 flowScenario:1 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:1 simu:SG energyModel:M2 key:NONE


