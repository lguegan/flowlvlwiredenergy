#+TITLE: /tmp/logs_microBenchmarks//SG-M1-1P_hete_1FS_1000BW_10LAT_1L_92238749flowSize-REGULAR.org
#+DATE: Wed Dec 19 10:32:10 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-12.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 38028 172.16.98.12 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=16
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 38028 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-e67d7e2e-8867-47de-b6c0-9230aefb9f50.xml 1 1 92238749 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.002868" />
		</link>

	<route src="Node0" dst="Node1" symmetrical="NO">
		<link_ctn id="Link1_UP" />
	</route>
	<route src="Node1" dst="Node0" symmetrical="NO">
		<link_ctn id="Link1_DOWN" />
	</route>



	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 1
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(1) 14.204616] [simulator/INFO] Node0 sent 92238749 bytes to Node1 in 14.204616 seconds from 0.000000 to 14.204616 with flow id 1
[14.204616] [link_energy/INFO] Total energy over all links: 32.508714
[14.204616] [simulator/INFO] ----- Simulation end -----
[14.204616] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 15.942045 Joules
[14.204616] [link_energy/INFO] Energy consumption of link 'Link1_UP': 16.566669 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.01 wallClock:0.10 peakMemUsage:62000 latency:10 execTime:0.09 simTime:14.204616 totalEnergy:32.508714 totalByteSend:0 flowSize:92238749 flowScenario:1 BW:1000 loop:1 idlecons: byteCons: consType:hete platform:1 simu:SG energyModel:M1 key:REGULAR


