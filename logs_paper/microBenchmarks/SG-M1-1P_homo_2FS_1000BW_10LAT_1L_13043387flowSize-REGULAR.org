#+TITLE: /tmp/logs_microBenchmarks//SG-M1-1P_homo_2FS_1000BW_10LAT_1L_13043387flowSize-REGULAR.org
#+DATE: Wed Dec 19 10:13:05 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-3.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 45204 172.16.98.3 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 45204 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-aa457496-a802-4811-9093-d6c04ebe2743.xml 2 1 13043387 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.002868" />
		</link>

	<route src="Node0" dst="Node1" symmetrical="NO">
		<link_ctn id="Link1_UP" />
	</route>
	<route src="Node1" dst="Node0" symmetrical="NO">
		<link_ctn id="Link1_DOWN" />
	</route>



	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 2
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(2) 2.120363] [simulator/INFO] Node0 sent 13043387 bytes to Node1 in 2.120363 seconds from 0.000000 to 2.120363 with flow id 2
[Node0:Node0:(1) 2.120363] [simulator/INFO] Node0 sent 13043387 bytes to Node1 in 2.120363 seconds from 0.000000 to 2.120363 with flow id 1
[2.120363] [link_energy/INFO] Total energy over all links: 4.955721
[2.120363] [simulator/INFO] ----- Simulation end -----
[2.120363] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 2.384621 Joules
[2.120363] [link_energy/INFO] Energy consumption of link 'Link1_UP': 2.571100 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.07 cpuTimeS:0.04 wallClock:1.04 peakMemUsage:60360 latency:10 execTime:0.11 simTime:2.120363 totalEnergy:4.955721 totalByteSend:0 flowSize:13043387 flowScenario:2 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:1 simu:SG energyModel:M1 key:REGULAR


