#+TITLE: /tmp/logs_microBenchmarks//SG-M1-3P_hete_3FS_1000BW_10LAT_1L_99718665flowSize-M1OP.org
#+DATE: Wed Dec 19 10:17:40 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-8.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 38204 172.16.98.8 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 38204 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-71c4b474-adc4-4101-bea8-0d8c6b5e8231.xml 3 1 99718665 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node2" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node3" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node4" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node5" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>
		<link id="Link2" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link3" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link4" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link5" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.53:4.28067" />
		</link>



		<route src="Node0" dst="Node4" symmetrical="NO">
			<link_ctn id="Link1_UP" />
			<link_ctn id="Link3_UP" />
			<link_ctn id="Link4_UP" />
		</route>
		<route src="Node4" dst="Node0" symmetrical="NO">
			<link_ctn id="Link1_DOWN" />
			<link_ctn id="Link3_DOWN" />
			<link_ctn id="Link4_DOWN" />
		</route>
		
		<route src="Node1" dst="Node5" symmetrical="NO">
			<link_ctn id="Link2_UP" />
			<link_ctn id="Link3_UP" />
			<link_ctn id="Link5_UP" />
		</route>
		<route src="Node5" dst="Node1" symmetrical="NO">
			<link_ctn id="Link2_DOWN" />
			<link_ctn id="Link3_DOWN" />
			<link_ctn id="Link5_DOWN" />
		</route>
	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 3
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node4:Node4:(3) 46.037882] [simulator/INFO] Node4 sent 99718665 bytes to Node0 in 46.037882 seconds from 0.000000 to 46.037882 with flow id 2
[Node0:Node0:(1) 46.037882] [simulator/INFO] Node0 sent 99718665 bytes to Node4 in 46.037882 seconds from 0.000000 to 46.037882 with flow id 1
[Node5:Node5:(7) 46.037882] [simulator/INFO] Node5 sent 99718665 bytes to Node1 in 46.037882 seconds from 0.000000 to 46.037882 with flow id 4
[Node1:Node1:(5) 46.037882] [simulator/INFO] Node1 sent 99718665 bytes to Node5 in 46.037882 seconds from 0.000000 to 46.037882 with flow id 3
[46.037882] [link_energy/INFO] Total energy over all links: 403.298983
[46.037882] [simulator/INFO] ----- Simulation end -----
[46.037882] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 52.308276 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link1_UP': 52.308276 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link2_DOWN': 39.938457 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link2_UP': 39.938457 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link3_DOWN': 41.895662 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link3_UP': 41.895662 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link4_DOWN': 39.938457 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link4_UP': 39.938457 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link5_DOWN': 27.568639 Joules
[46.037882] [link_energy/INFO] Energy consumption of link 'Link5_UP': 27.568639 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.00 wallClock:0.10 peakMemUsage:61528 latency:10 execTime:0.08 simTime:46.037882 totalEnergy:403.298983 totalByteSend:0 flowSize:99718665 flowScenario:3 BW:1000 loop:1 idlecons: byteCons: consType:hete platform:3 simu:SG energyModel:M1 key:M1OP


