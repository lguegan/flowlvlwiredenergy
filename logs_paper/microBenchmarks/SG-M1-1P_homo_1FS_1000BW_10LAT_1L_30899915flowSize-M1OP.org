#+TITLE: /tmp/logs_microBenchmarks//SG-M1-1P_homo_1FS_1000BW_10LAT_1L_30899915flowSize-M1OP.org
#+DATE: Wed Dec 19 10:13:06 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-24.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 51602 172.16.98.24 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 51602 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-75c1245b-db68-4c04-bac0-4128dbab5e81.xml 1 1 30899915 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>

	<route src="Node0" dst="Node1" symmetrical="NO">
		<link_ctn id="Link1_UP" />
	</route>
	<route src="Node1" dst="Node0" symmetrical="NO">
		<link_ctn id="Link1_DOWN" />
	</route>



	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 1
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(1) 4.845053] [simulator/INFO] Node0 sent 30899915 bytes to Node1 in 4.845053 seconds from 0.000000 to 4.845053 with flow id 1
[4.845053] [link_energy/INFO] Total energy over all links: 11.088399
[4.845053] [simulator/INFO] ----- Simulation end -----
[4.845053] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 5.437673 Joules
[4.845053] [link_energy/INFO] Energy consumption of link 'Link1_UP': 5.650726 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.00 wallClock:0.10 peakMemUsage:61784 latency:10 execTime:0.08 simTime:4.845053 totalEnergy:11.088399 totalByteSend:0 flowSize:30899915 flowScenario:1 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:1 simu:SG energyModel:M1 key:M1OP


