#+TITLE: /tmp/logs_microBenchmarks//SG-M1-3P_hete_2FS_1000BW_10LAT_1L_92238749flowSize-M1OP.org
#+DATE: Wed Dec 19 10:32:12 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-12.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 38028 172.16.98.12 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=16
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 38028 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-cdbb6921-453e-4873-9f8d-ac7997e99c9b.xml 2 1 92238749 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node2" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node3" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node4" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node5" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>
		<link id="Link2" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link3" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link4" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.825:3.14177" />
		</link>
		<link id="Link5" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.53:4.28067" />
		</link>



		<route src="Node0" dst="Node4" symmetrical="NO">
			<link_ctn id="Link1_UP" />
			<link_ctn id="Link3_UP" />
			<link_ctn id="Link4_UP" />
		</route>
		<route src="Node4" dst="Node0" symmetrical="NO">
			<link_ctn id="Link1_DOWN" />
			<link_ctn id="Link3_DOWN" />
			<link_ctn id="Link4_DOWN" />
		</route>
		
		<route src="Node1" dst="Node5" symmetrical="NO">
			<link_ctn id="Link2_UP" />
			<link_ctn id="Link3_UP" />
			<link_ctn id="Link5_UP" />
		</route>
		<route src="Node5" dst="Node1" symmetrical="NO">
			<link_ctn id="Link2_DOWN" />
			<link_ctn id="Link3_DOWN" />
			<link_ctn id="Link5_DOWN" />
		</route>
	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 2
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(2) 42.613848] [simulator/INFO] Node0 sent 92238749 bytes to Node4 in 42.613848 seconds from 0.000000 to 42.613848 with flow id 2
[Node0:Node0:(1) 42.613848] [simulator/INFO] Node0 sent 92238749 bytes to Node4 in 42.613848 seconds from 0.000000 to 42.613848 with flow id 1
[Node1:Node1:(6) 42.613848] [simulator/INFO] Node1 sent 92238749 bytes to Node5 in 42.613848 seconds from 0.000000 to 42.613848 with flow id 2
[Node1:Node1:(5) 42.613848] [simulator/INFO] Node1 sent 92238749 bytes to Node5 in 42.613848 seconds from 0.000000 to 42.613848 with flow id 1
[42.613848] [link_energy/INFO] Total energy over all links: 373.303916
[42.613848] [simulator/INFO] ----- Simulation end -----
[42.613848] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 47.793260 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link1_UP': 49.042512 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link2_DOWN': 35.328962 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link2_UP': 38.607166 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link3_DOWN': 35.501499 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link3_UP': 42.057907 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link4_DOWN': 35.328962 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link4_UP': 38.607166 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link5_DOWN': 22.864664 Joules
[42.613848] [link_energy/INFO] Energy consumption of link 'Link5_UP': 28.171820 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.01 wallClock:0.09 peakMemUsage:61616 latency:10 execTime:0.09 simTime:42.613848 totalEnergy:373.303916 totalByteSend:0 flowSize:92238749 flowScenario:2 BW:1000 loop:1 idlecons: byteCons: consType:hete platform:3 simu:SG energyModel:M1 key:M1OP


