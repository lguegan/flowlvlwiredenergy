#+TITLE: /tmp/logs_microBenchmarks//SG-M1-2P_homo_4FS_1000BW_10LAT_1L_84878011flowSize-M1OP.org
#+DATE: Wed Dec 19 10:13:05 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-25.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 40274 172.16.98.25 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 40274 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-d59e23e3-622e-45e0-a62b-b2146ed8ed44.xml 4 1 84878011 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node2" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node3" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link1" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>
		<link id="Link2" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>
		<link id="Link3" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="1.12:2.00287" />
		</link>

		<route src="Node0" dst="Node3" symmetrical="NO">
			<link_ctn id="Link1_UP" />
			<link_ctn id="Link2_UP" />
			<link_ctn id="Link3_UP" />
		</route>
		<route src="Node3" dst="Node0" symmetrical="NO">
			<link_ctn id="Link1_DOWN" />
			<link_ctn id="Link2_DOWN" />
			<link_ctn id="Link3_DOWN" />
		</route>

	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 4
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node0:Node0:(2) 39.244370] [simulator/INFO] Node0 sent 84878011 bytes to Node3 in 39.244370 seconds from 0.000000 to 39.244370 with flow id 2
[Node0:Node0:(1) 39.244370] [simulator/INFO] Node0 sent 84878011 bytes to Node3 in 39.244370 seconds from 0.000000 to 39.244370 with flow id 1
[Node3:Node3:(4) 39.244370] [simulator/INFO] Node3 sent 84878011 bytes to Node0 in 39.244370 seconds from 0.000000 to 39.244370 with flow id 3
[39.244370] [link_energy/INFO] Total energy over all links: 269.444255
[39.244370] [simulator/INFO] ----- Simulation end -----
[39.244370] [link_energy/INFO] Energy consumption of link 'Link1_DOWN': 44.619758 Joules
[39.244370] [link_energy/INFO] Energy consumption of link 'Link1_UP': 45.194994 Joules
[39.244370] [link_energy/INFO] Energy consumption of link 'Link2_DOWN': 44.619758 Joules
[39.244370] [link_energy/INFO] Energy consumption of link 'Link2_UP': 45.194994 Joules
[39.244370] [link_energy/INFO] Energy consumption of link 'Link3_DOWN': 44.619758 Joules
[39.244370] [link_energy/INFO] Energy consumption of link 'Link3_UP': 45.194994 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.08 cpuTimeS:0.00 wallClock:0.10 peakMemUsage:61776 latency:10 execTime:0.08 simTime:39.244370 totalEnergy:269.444255 totalByteSend:0 flowSize:84878011 flowScenario:4 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:2 simu:SG energyModel:M1 key:M1OP


