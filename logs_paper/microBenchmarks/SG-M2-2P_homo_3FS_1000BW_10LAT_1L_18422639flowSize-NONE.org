#+TITLE: /tmp/logs_microBenchmarks//SG-M2-2P_homo_3FS_1000BW_10LAT_1L_18422639flowSize-NONE.org
#+DATE: Wed Dec 19 10:13:06 CET 2018
#+AUTHOR: lguegan

* System informations
Linux parapide-7.rennes.grid5000.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.130-2 (2018-10-27) x86_64 GNU/Linux

* Environment variables
LC_ALL=en_US.UTF-8
LD_LIBRARY_PATH=./libs/simgridMicroBenchmarks/lib/:./libs/ns3/build
SSH_CONNECTION=172.16.98.11 35956 172.16.98.7 6667
LANG=en_US.UTF-8
OARDO_USER=oar
OARDO_UID=113
XDG_SESSION_ID=5
USER=lguegan
PWD=/home/lguegan
HOME=/home/lguegan
OAR_CPUSET=/oar/lguegan_1085810
SSH_CLIENT=172.16.98.11 35956 6667
OARCONFFILE=/etc/oar/oar.conf
SHELL=/bin/bash
OAR_JOB_USER=lguegan
PERL5LIB=/usr/lib/oar
SHLVL=2
LANGUAGE=en_US:en
LOGNAME=lguegan
XDG_RUNTIME_DIR=/run/user/113
OARDIR=/usr/lib/oar
PATH=/usr/lib/oar/oardodo:/usr/local/bin:/usr/bin:/bin:/usr/games
OARUSER=oar
OARXAUTHLOCATION=/usr/bin/xauth
_=/usr/bin/env

* Complete command
././1-microBenchmarks//simulator /tmp/logs_microBenchmarks//platform-98ffe14d-ace0-4e36-a3ac-ad627cb2aeb8.xml 3 1 18422639 --cfg=network/model:LV08 --cfg=network/TCP-gamma:131072

* Platform file
<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
	<AS id="AS0" routing="Full">
		<host id="Node0" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node1" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node2" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>
		<host id="Node3" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0">
		</host>

		<link id="Link11" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link12" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link13" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>

		<link id="Link21" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link22" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link23" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>

		<link id="Link31" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>
		<link id="Link32" bandwidth="1000Mbps" latency="10ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0:0" />
		</link>
		<link id="Link33" bandwidth="1000Mbps" latency="0ms"
			sharing_policy="SPLITDUPLEX">
			<prop id="watt_range" value="0.56:1.001434" />
		</link>


		<route src="Node0" dst="Node3" symmetrical="NO">
			<link_ctn id="Link11_UP" />
			<link_ctn id="Link12_UP" />
			<link_ctn id="Link13_UP" />
			<link_ctn id="Link21_UP" />
			<link_ctn id="Link22_UP" />
			<link_ctn id="Link23_UP" />
			<link_ctn id="Link31_UP" />
			<link_ctn id="Link32_UP" />
			<link_ctn id="Link33_UP" />
		</route>
		<route src="Node3" dst="Node0" symmetrical="NO">
			<link_ctn id="Link11_DOWN" />
			<link_ctn id="Link12_DOWN" />
			<link_ctn id="Link13_DOWN" />
			<link_ctn id="Link21_DOWN" />
			<link_ctn id="Link22_DOWN" />
			<link_ctn id="Link23_DOWN" />
			<link_ctn id="Link31_DOWN" />
			<link_ctn id="Link32_DOWN" />
			<link_ctn id="Link33_DOWN" />
		</route>


	</AS>
</platform>
* Output
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'LV08'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP-gamma' to '131072'
[0.000000] [simulator/INFO] Init hosts using flow scenario 3
[0.000000] [simulator/INFO] ----- Simulation start -----
[Node3:Node3:(3) 8.823515] [simulator/INFO] Node3 sent 18422639 bytes to Node0 in 8.823515 seconds from 0.000000 to 8.823515 with flow id 2
[Node0:Node0:(1) 8.823515] [simulator/INFO] Node0 sent 18422639 bytes to Node3 in 8.823515 seconds from 0.000000 to 8.823515 with flow id 1
[8.823515] [link_energy/INFO] Total energy over all links: 60.151703
[8.823515] [simulator/INFO] ----- Simulation end -----
[8.823515] [link_energy/INFO] Energy consumption of link 'Link11_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link11_UP': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link12_DOWN': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link12_UP': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link13_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link13_UP': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link21_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link21_UP': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link22_DOWN': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link22_UP': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link23_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link23_UP': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link31_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link31_UP': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link32_DOWN': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link32_UP': 0.000000 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link33_DOWN': 5.012642 Joules
[8.823515] [link_energy/INFO] Energy consumption of link 'Link33_UP': 5.012642 Joules

* Metrics
execStatus:0 swappedCount:0 cpuTimeU:0.07 cpuTimeS:0.01 wallClock:0.09 peakMemUsage:61744 latency:10 execTime:0.08 simTime:8.823515 totalEnergy:60.151703 totalByteSend:0 flowSize:18422639 flowScenario:3 BW:1000 loop:1 idlecons: byteCons: consType:homo platform:2 simu:SG energyModel:M2 key:NONE


